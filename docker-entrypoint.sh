#!/bin/ash

if [ "supervisord" == "$(basename $1)" ]; then
  if [ ! -f /etc/nginx/auth/htpasswd ]; then
    sed -i -e "s|--passwords=.*|--passwords=.|" \
      -e "s|--authenticate=.*|--authenticate=.|" /etc/supervisor.d/pypiserver.ini
    sed -i "/auth_basic/d" /etc/nginx/conf.d/default.conf
  fi
fi

exec "$@"
