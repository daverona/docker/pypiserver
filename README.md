# daverona/pypiserver

[![pipeline status](https://gitlab.com/daverona/docker/pypiserver/badges/master/pipeline.svg)](https://gitlab.com/daverona/docker/pypiserver/-/commits/master)

This is a repository for Docker images of [pypiserver](https://pypi.org/project/pypiserver/).

* GitLab repository: [https://gitlab.com/daverona/docker/pypiserver](https://gitlab.com/daverona/docker/pypiserver)
* Docker registry: [https://hub.docker.com/r/daverona/pypiserver](https://hub.docker.com/r/daverona/pypiserver)
* Available releases: [https://gitlab.com/daverona/docker/pypiserver/-/releases](https://gitlab.com/daverona/docker/pypiserver/-/releases)

## Quick Start

```bash
docker container run --rm \
  --detach \
  --publish 80:80 \
  --volume "${PWD}/packages:/packages" \
  --name pypiserver \
  daverona/pypiserver
```

Visit [http://localhost](http://localhost) and observe
the pypiserver has no package to serve.
To verify that pypiserver is working, make a `.pypirc` in your home directory first:

```
[distutils]
index-servers=
  pypiserver

[pypiserver]
repository:http://localhost
username:bogus
password:bogus
```

> Although pypiserver does not authenticate its users, `.pypirc` is required even with bogus username and password.

Make a sample package of, say [requests](https://requests.readthedocs.io/en/master/), and upload:

```bash
git clone https://github.com/psf/requests
cd requests
python3 setup.py sdist upload --repository pypiserver
```

Visit [http://localhost](http://localhost) again and observe
our pypiserver has a single package to serve, namely requests package.
Install the uploaded requests package from pypiserver:

```bash
pip3 install --index-url http://localhost/simple/ requests
```

## Usage

To protect pypiserver, make a username and password pair first:

```bash
docker container run --rm daverona/pypiserver \ 
  htpasswd -n admin \
>> htpasswd
# enter password, say "secret", for username "admin"
```

Create `htpasswd` file and append the above output to `htpasswd` file.

> The authentication is for listing, uploading, and updating packages.
> Each user who wants to use pypiserver needs a valid username and password pair.
> Do NOT share username/password among pypiserver users.
> Instead repeat the above step for each user.

Run pypiserver with `htpasswd` file bind-mounted:

```bash
docker container run --rm \
  --detach \
  --publish 80:80 \
  --volume "${PWD}/htpasswd:/etc/nginx/auth/htpasswd:ro" \
  --volume "${PWD}/packages:/packages" \
  --name pypiserver \
  daverona/pypiserver
```

Each pipyserver user need to create `.pypirc` under home directory with
his/her own username and password:

```
[distutils]
index-servers= 
  pypiserver

[pypiserver]
repository:http://localhost
username:admin
password:secret
```

<!--
## Advanced Usage

To protect the whole website you can use the existing `htpasswd` file for basic authentication.
Create a file, say `extra.conf`, under `${PYPISERVER_STORAGE}/pypiserver/conf` with the following:

```nginx
auth_basic "Protected Area";
auth_basic_user_file auth/htpasswd;
```

and mount it to `/etc/nginx/conf.d`:

```yaml
volumes:
  ...
  - "${PYPISERVER_STORAGE}/pypiserver/conf/extra.conf:/etc/nginx/conf.d/extra.conf:ro"
```

> Other options (e.g. `client_max_body_size`) can be specified in `extra.conf`.
-->

## References

* pypiserver: [https://pypi.org/project/pypiserver/](https://pypi.org/project/pypiserver/)
* pypiserver/pypiserver: [https://github.com/pypiserver/pypiserver](https://github.com/pypiserver/pypiserver)
* praekeltfoundation/docker-pypiserver: [https://github.com/praekeltfoundation/docker-pypiserver](https://github.com/praekeltfoundation/docker-pypiserver)

