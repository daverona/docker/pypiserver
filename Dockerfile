FROM alpine:3.8

ARG PYPISERVER_VERSION=v1.3.2

RUN apk add --no-cache \
    apache2-utils \
    logrotate \
    nginx \
    nginx-mod-http-headers-more \
    py3-bcrypt \
    py3-cffi \
    py3-six \
    python3 \
    supervisor \
    tzdata \
  && python3 -m pip install --no-cache-dir --upgrade pip \
  && pip3 install --no-cache-dir --upgrade setuptools wheel \
  # Install pypiserver
  && pip3 install --no-cache-dir "pypiserver[cache,passlib]==${PYPISERVER_VERSION}" \
  # Configure pypiserver
  && mkdir -p /var/log/pypiserver \
  && mkdir -p /packages \
  # Configure nginx
  && mkdir -p /var/run/nginx \
  && mkdir -p /etc/nginx/auth /etc/nginx/cache \
  # @see https://github.com/openresty/lua-nginx-module/issues/1461#issuecomment-464504397
  && sed -i "s|log_format main|log_format main escape=none|" /etc/nginx/nginx.conf

COPY logrotate/ /etc/logrotate.d/
COPY nginx/ /etc/nginx/conf.d/
COPY supervisor/ /etc/supervisor.d/

COPY docker-entrypoint.sh /
RUN chmod a+x /docker-entrypoint.sh
EXPOSE 80/tcp
WORKDIR /packages

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["supervisord", "-c", "/etc/supervisord.conf", "-n"]
